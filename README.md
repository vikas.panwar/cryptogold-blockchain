**Steps to deploy the cryptogold blockchain over docker container:-**

1)Pull the docker image <br/>
 docker pull <image-name>

2)See the image installed locally<br/>
 docker images

3)Run the pulled image to the container<br/> 
docker container run -d -p <local-port>:<container-port> --name <container-name> -i -t <image-name> /bin/bash

4)See the running image on the container<br/>
docker ps

5)Access your container <br/>
docker exec -it <conatainer-id> /bin/bash

6)First install the vim editor to change the config file.<br/>
apt-get update
apt-get install vim

7)Check the ip of your container.In my case it was "172.17.0.2"<br/>
docker inspect <container-id> | grep "IPAddress"

8)Open the config file<br/>
vim config.json 

9)Now enter the ip address of your container in the config file<br/>
"access": {
      "whiteList": [
        "127.0.0.1",
        <container-ip>
      ]
    }

10)change the admin address.
 "admin":"AHPj9iVkjYtk2SqV2pvvGTZ2dLXKwNPiHc"

11)Save the changes and exit from config

12)Run the application. Blockchain will start on the container ip i.e <container-ip>:4096<br/>
node app.js

**Your main peer has been created.**

**Steps to create another peers:**

12)Now generate the addresses over the blockchain that you will use to create another peers<br/>
curl -k -H "Content-Type: application/json" -X POST -d '{"secret":"fault still attack alley expand music basket purse later educate follow ride"}' 'http://<container-ip>:4096/api/accounts/generatePublickey'

13)Now change the tag of the image that you pulled earlier<br/>
docker tag <image-id> <new-tag-name>

14)Repeat the step 3

15)Repeat the step 4

16)Repeat the step 5

17)Repeat the step 6

18)Check the ip of your container and the ip of the main peer <br/>
docker inspect <container-id> | grep "IPAddress"

19)Open the config file<br/>
vim config.json

20)Enter ip address and port of main peer container.<br/>
  "list": [{
    "ip":<main-peer-ip>,
    "port":<port>
  }]

21)Remove all the secrets from the file and enter the secret that you generated in step 12. 

22)Now enter the ip address of your container and main peer in the config file<br/>
"access": {
      "whiteList": [
        "127.0.0.1",
        <container-ip>,
        <main-peer-ip>
      ]
    }

23)Save the file and exit.

24)Run the application and you will see that your peer will start syncing the blocks.<br/>
node app.js




